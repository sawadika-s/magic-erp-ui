/**
 * 库存报损相关API
 */

import request from '@/utils/request'

/**
 * 归还库存报损
 * @param id
 * @param parmas
 */
export function returnGoodsInventoryLoss(id, parmas) {
  return request({
    url: `admin/stock/damage/report/return/${id}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 批量确认库存报损单
 * @param id
 */
export function InventoryLossFormConfirm(ids) {
  return request({
    url: `admin/stock/damage/report/confirm/${ids}`,
    method: 'post'
  })
}

/**
 * 修改库存报损
 * @param id
 * @param parmas
 */
export function editgoodsInventoryLossAdd(id, parmas) {
  return request({
    url: `admin/stock/damage/report/${id}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 添加库存报损
 * @param params
 */
export function addgoodsInventoryLossAdd(params) {
  return request({
    url: 'admin/stock/damage/report',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 提交审核库存报损单
 * @param ids
 */
export function submitAuditGoodsInventoryLoss(ids) {
  return request({
    url: `admin/stock/damage/report/submit/${ids}`,
    method: 'post'
  })
}

/**
 * 撤回库存报损
 * @param ids
 */
export function cancelGoodsInventoryLoss(ids) {
  return request({
    url: `admin/stock/damage/report/cancel/${ids}`,
    method: 'post'
  })
}

/**
 * 审核库存报损
 * @param params
 */
export function auditGoodsInventoryLoss(ids, parmas) {
  return request({
    url: `admin/stock/damage/report/audit/${ids}`,
    method: 'post',
    data: parmas
  })
}

/**
 * 获取库存报损详情
 * @param params
 */
export function getGoodsInventoryLossInfo(id) {
  return request({
    url: `admin/stock/damage/report/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 获取库存报损列表
 * @param params
 */
export function getGoodsInventoryLossList(params) {
  return request({
    url: 'admin/stock/damage/report',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 更新库存报损
 * @param id
 * @param parmas
 */
export function editGoodsInventoryLoss(id, parmas) {
  return request({
    url: `admin/stock/damage/report/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除库存报损
 * @param id
 */
export function deleteGoodsInventoryLoss(ids) {
  return request({
    url: `admin/stock/damage/report/${ids}`,
    method: 'delete'
  })
}
