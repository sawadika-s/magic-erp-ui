/**
 * 商品借出相关API
 */

import request from '@/utils/request'

/**
 * 归还商品借出
 * @param id
 * @param parmas
 */
export function returnGoodsLend(id, parmas) {
  return request({
    url: `admin/lend/form/return/${id}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 批量确认商品借出单
 * @param id
 */
export function lendFormConfirm(ids) {
  return request({
    url: `admin/lend/form/confirm/${ids}`,
    method: 'post'
  })
}

/**
 * 获取商品借出列表
 * @param params
 */
export function getgoodsLendAddList(params) {
  return request({
    url: 'admin/lend/form',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 修改商品借出
 * @param id
 * @param parmas
 */
export function editgoodsLendAdd(id, parmas) {
  return request({
    url: `admin/lend/form/${id}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 添加商品借出
 * @param params
 */
export function addgoodsLendAdd(params) {
  return request({
    url: 'admin/lend/form',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 删除商品借出
 * @param id
 */
export function deletegoodsLendAdd(id) {
  return request({
    url: `admin/lend/form/${id}`,
    method: 'delete'
  })
}

/**
 * 商品借出详情
 * @param id
 */
export function getgoodsLendAddInfo(id) {
  return request({
    url: `admin/lend/form/${id}`,
    method: 'get'
  })
}

/**
 * 审核商品借出
 * @param params
 */
export function auditGoodsLend(id, parmas) {
  return request({
    url: `admin/lend/form/${id}/audit`,
    method: 'post',
    data: parmas
  })
}

/**
 * 获取商品借出详情
 * @param params
 */
export function getGoodsLendInfo(id) {
  return request({
    url: `admin/lend/form/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 查询待归还的借出单商品分页列表数据
 * @param params
 */
export function getGoodsLendListWaitReturn(params) {
  return request({
    url: 'admin/lend/form/list-wait-return',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 获取商品借出列表
 * @param params
 */
export function getGoodsLendList(params) {
  return request({
    url: 'admin/lend/form',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 添加商品借出
 * @param params
 */
export function addGoodsLend(parmas) {
  return request({
    url: `admin/lend/form`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 更新商品借出
 * @param id
 * @param parmas
 */
export function editGoodsLend(id, parmas) {
  return request({
    url: `admin/lend/form/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除商品借出
 * @param id
 */
export function deleteGoodsLend(ids) {
  return request({
    url: `admin/lend/form/${ids}`,
    method: 'delete'
  })
}
