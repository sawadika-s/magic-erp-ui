/**
 * 企业管理相关API
 */

import request from '@/utils/request'

/**
 * 分页列表
 * @param params
 */
export function getList(params) {
  return request({
    url: '/admin/erp/enterprise',
    method: 'get',
    params
  })
}

/**
 * 添加
 * @param params
 */
export function add(params) {
  return request({
    url: '/admin/erp/enterprise',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 查询详情
 * @param id
 */
export function getDetail(id) {
  return request({
    url: `admin/erp/enterprise/${id}`,
    method: 'get'
  })
}

/**
 * 修改企业
 * @param id
 * @param params
 */
export function edit(id, params) {
  return request({
    url: `admin/erp/enterprise/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 删除
 * @param id
 */
export function del(id) {
  return request({
    url: `admin/erp/enterprise/${id}`,
    method: 'delete'
  })
}
