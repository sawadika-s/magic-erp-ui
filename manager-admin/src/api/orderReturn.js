/**
 * 订单退货相关API
 */

import request from '@/utils/request'

/**
 * 获取出库单商品明细
 * @param params
 * @returns {Promise<any>}
 */
export function getWarehouseOutProduct(params) {
  return request({
    url: 'admin/erp/warehouseOutItem',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 提交订单退货退货
 * @param params
 */
export function submitorderReturn(ids) {
  return request({
    url: `admin/erp/orderReturn/${ids}/submit`,
    method: 'post'
  })
}
/**
 * 撤回订单退货退货
 * @param params
 */
export function withdraworderReturn(id) {
  return request({
    url: `admin/erp/orderReturn/${id}/withdraw`,
    method: 'post'
  })
}
/**
 * 审核订单退货退货
 * @param params
 */
export function auditorderReturn(id, parmas) {
  return request({
    url: `admin/erp/orderReturn/${id}/audit`,
    method: 'post',
    data: parmas
  })
}

/**
 * 获取订单退货退货详情
 * @param params
 */
export function getorderReturnInfo(id) {
  return request({
    url: `admin/erp/orderReturn/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 获取订单退货退货列表
 * @param params
 */
export function getorderReturnList(params) {
  return request({
    url: 'admin/erp/orderReturn',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 添加订单退货退货
 * @param params
 */
export function addorderReturn(parmas) {
  return request({
    url: `admin/erp/orderReturn`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 更新订单退货退货
 * @param id
 * @param parmas
 */
export function editorderReturn(id, parmas) {
  return request({
    url: `admin/erp/orderReturn/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除订单退货退货
 * @param id
 */
export function deleteorderReturn(ids) {
  return request({
    url: `admin/erp/orderReturn/${ids}`,
    method: 'delete'
  })
}
