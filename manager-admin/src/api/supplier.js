/**
 * 供应商相关API
 */

import request from '@/utils/request'

/**
 * 供应商结算单明细
 * @param id
 */
export function getsupplierSettlementItem(params) {
  return request({
    url: `admin/erp/supplierSettlementItem`,
    method: 'get',
    params
  })
}

/**
 * 获取结算单详情
 * @param id
 */
export function getsupplierSettlementInfo(id) {
  return request({
    url: `admin/erp/supplierSettlement/${id}`,
    method: 'get'
  })
}

/**
 * 取消结算单
 * @param id
 */
export function deletesupplierSettlementCancel(id) {
  return request({
    url: `admin/erp/supplierSettlement/${id}/cancel`,
    method: 'delete'
  })
}

/**
 * 获取结算单列表
 * @param params
 */
export function getsupplierSettlement(params) {
  return request({
    url: 'admin/erp/supplierSettlement',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 生成结算单
 * @param params
 */
export function adderpSupplierSettlement(parmas) {
  return request({
    url: `admin/erp/supplierSettlement`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 提交供应商退货
 * @param params
 */
export function submitsupplierReturns(ids) {
  return request({
    url: `admin/erp/supplierReturn/${ids}/submit`,
    method: 'post'
  })
}
/**
 * 撤回供应商退货
 * @param params
 */
export function withdrawsupplierReturns(id) {
  return request({
    url: `admin/erp/supplierReturn/${id}/withdraw`,
    method: 'post'
  })
}
/**
 * 审核供应商退货
 * @param params
 */
export function auditsupplierReturns(id, parmas) {
  return request({
    url: `admin/erp/supplierReturn/${id}/audit`,
    method: 'post',
    data: parmas
  })
}

/**
 * 获取供应商退货详情
 * @param params
 */
export function getsupplierReturnsInfo(id) {
  return request({
    url: `admin/erp/supplierReturn/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 获取供应商退货列表
 * @param params
 */
export function getsupplierReturnsList(params) {
  return request({
    url: 'admin/erp/supplierReturn',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 添加供应商退货
 * @param params
 */
export function addsupplierReturns(parmas) {
  return request({
    url: `admin/erp/supplierReturn`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 更新供应商退货
 * @param id
 * @param parmas
 */
export function editsupplierReturns(id, parmas) {
  return request({
    url: `admin/erp/supplierReturn/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除供应商退货
 * @param id
 */
export function deletesupplierReturns(ids) {
  return request({
    url: `admin/erp/supplierReturn/${ids}`,
    method: 'delete'
  })
}
