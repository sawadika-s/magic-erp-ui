/**
 * 会员管理相关API
 */

import request from '@/utils/request'
import md5 from 'js-md5'

/**
 * 获取会员列表
 * @param params
 */
export function getMemberList(params) {
  return request({
    url: '/admin/erp/member',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 添加会员
 * @param params
 */
export function addMember(params) {
  return request({
    url: '/admin/erp/member',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 获取会员详情
 * @param id
 */
export function getMemberDetail(id) {
  return request({
    url: `admin/erp/member/${id}`,
    method: 'get'
  })
}

/**
 * 修改会员
 * @param id
 * @param parmas
 */
export function editMember(id, parmas) {
  return request({
    url: `admin/erp/member/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除会员
 * @param id
 */
export function deleteMember(id) {
  return request({
    url: `admin/erp/member/${id}`,
    method: 'delete'
  })
}

/**
 * 禁用会员
 * @param id
 */
export function disableMember(id) {
  return request({
    url: `admin/erp/member/${id}/disable`,
    method: 'put'
  })
}

/**
 * 启用会员
 * @param id
 */
export function enableMember(id) {
  return request({
    url: `admin/erp/member/${id}/enable`,
    method: 'put'
  })
}
