/**
 * 基础设置相关API
 */

import request from '@/utils/request'

/**
 * 添加门店
 * @param store
 */
export function addStore(store) {
  return request({
    url: '/admin/erp/store',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: store
  })
}

/**
 * 编辑门店
 * @param store
 */
export function editStore(id, store) {
  return request({
    url: `/admin/erp/store/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: store
  })
}

/**
 * 删除门店
 * @param id
 */
export function deleteStore(id) {
  return request({
    url: `/admin/erp/store/${id}`,
    method: 'delete'
  })
}

/**
 * 获取门店列表
 * @param params
 */
export function getBranches(params) {
  return request({
    url: '/admin/erp/store',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 获取品牌列表
 * @param params
 * @returns {Promise<any>}
 */
export function getBrandList(params) {
  return request({
    url: 'admin/goods/brands',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 获取所有品牌数据
 * @returns
 */
export function getBrandAllList() {
  return request({
    url: 'admin/goods/brands/all',
    method: 'get',
    loading: false
  })
}

/**
 * 添加品牌
 * @param params
 */
export function addBrand(params) {
  return request({
    url: 'admin/goods/brands',
    method: 'post',
    data: params
  })
}

/**
 * 编辑品牌
 * @param id
 * @param params
 */
export function editBrand(id, params) {
  return request({
    url: `admin/goods/brands/${id}`,
    method: 'put',
    data: params
  })
}

/**
 * 删除品牌
 * @param ids
 * @returns {Promise<any>}
 */
export function deleteBrand(ids) {
  if (Array.isArray(ids)) ids = ids.join(',')
  return request({
    url: `admin/goods/brands/${ids}`,
    method: 'delete'
  })
}

/**
 * 修改营销经理
 * @param id
 * @param parmas
 */
export function editMarketingManager(id, parmas) {
  return request({
    url: `admin/erp/marketingManager/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 新增营销经理
 * @param id
 * @param parmas
 */
export function addMarketingManager(parmas) {
  return request({
    url: `admin/erp/marketingManager`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 获取营销经理列表
 * @param params
 */
export function getMarketingManagerList(params) {
  return request({
    url: 'admin/erp/marketingManager',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 获取编号规则列表
 * @param params
 */
export function getNoGenerateRuleList(params) {
  return request({
    url: 'admin/erp/noGenerateRule',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 修改编号规则
 * @param id
 * @param parmas
 */
export function editNoGenerateRule(id, parmas) {
  return request({
    url: `admin/erp/noGenerateRule/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 添加编号规则
 * @param params
 */
export function addNoGenerateRule(params) {
  return request({
    url: 'admin/erp/noGenerateRule',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 删除编号规则
 * @param id
 */
export function deleteNoGenerateRule(id) {
  return request({
    url: `admin/erp/noGenerateRule/${id}`,
    method: 'delete'
  })
}

/**
 * 获取仓库列表
 * @param params
 */
export function getWarehouseList(params) {
  return request({
    url: 'admin/erp/warehouse',
    method: 'get',
    params
  })
}

/**
 * 修改仓库
 * @param id
 * @param parmas
 */
export function editWarehouse(id, parmas) {
  return request({
    url: `admin/erp/warehouse/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 添加仓库
 * @param params
 */
export function addWarehouse(params) {
  return request({
    url: 'admin/erp/warehouse',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 删除仓库
 * @param id
 */
export function deleteWarehouse(id) {
  return request({
    url: `admin/erp/warehouse/${id}`,
    method: 'delete'
  })
}

/**
 * 所有仓库
 * @param params
 */
export function getWarehouseListAll(params) {
  return request({
    url: 'admin/erp/warehouse/list-all',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 所有正常状态的供应商
 * @param params
 */
export function getSupplierListAll() {
  return request({
    url: 'admin/erp/supplier/list-all',
    method: 'get',
    loading: false
  })
}

/**
 * 获取供应商列表
 * @param params
 */
export function getSupplierList(params) {
  return request({
    url: 'admin/erp/supplier',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 修改供应商
 * @param id
 * @param parmas
 */
export function editSupplier(id, parmas) {
  return request({
    url: `admin/erp/supplier/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 添加供应商
 * @param params
 */
export function addSupplier(params) {
  return request({
    url: 'admin/erp/supplier',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 删除供应商
 * @param id
 */
export function deleteSupplier(id) {
  return request({
    url: `admin/erp/supplier/${id}`,
    method: 'delete'
  })
}
