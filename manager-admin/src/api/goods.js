/**
 * 商品换货相关API
 */

import request from '@/utils/request'

/**
 * 获取商品档案列表
 * @param params
 */
export function getGoodsList(params) {
  return request({
    url: 'admin/erp/goods',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 获取商品档案详情
 * @param id
 */
export function getGoodsInfo(id) {
  return request({
    url: `admin/erp/goods/${id}`,
    method: 'get',
    loading: false
  })
}

/**
 * 修改商品档案
 * @param id
 * @param parmas
 */
export function editGoods(id, parmas) {
  return request({
    url: `admin/erp/goods/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 添加商品档案
 * @param params
 */
export function addGoods(params) {
  return request({
    url: 'admin/erp/goods',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 删除商品档案
 * @param id
 */
export function deleteGoods(id) {
  return request({
    url: `admin/erp/goods/${id}`,
    method: 'delete'
  })
}

/**
 * 获取预警商品列表
 * @param params
 * @returns {Promise<any>}
 */
export function getWarningGoodsList(params) {
  return request({
    url: 'admin/erp/productStock/list-stock-warning',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 发布商品   查询商品参数，获取所选分类关联的参数信息
 * @param category_id
 * @returns {Promise<any>}
 */
export function getGoodsParams(category_id) {
  return request({
    url: `admin/goods/categories/${category_id}/params`,
    method: 'get',
    loading: false
  })
}

/**
 * 编辑商品   查询商品参数，获取所选分类关联的参数信息
 * @param category_id
 * @param goods_id
 * @returns {Promise<any>}
 */
export function getEditGoodsParams(category_id, goods_id) {
  return request({
    url: `admin/goods/categories/${category_id}/${goods_id}/params`,
    method: 'get',
    loading: false
  })
}

