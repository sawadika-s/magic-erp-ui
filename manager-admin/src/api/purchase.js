/**
 * 采购管理相关API
 */

import request from '@/utils/request'

/**
 * 根据采购计划ID查询采购计划商品信息集合
 * @param params
 */
export function getPurchaseProductList(id) {
  return request({
    url: `/admin/procurement/plan/product/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 提交采购入库
 * @param params
 */
export function submitPurchaseWarehousing(id) {
  return request({
    url: `admin/erp/warehouseEntry/${id}/submit`,
    method: 'post'
  })
}
/**
 * 撤回采购入库
 * @param params
 */
export function withdrawPurchaseWarehousing(id) {
  return request({
    url: `admin/erp/warehouseEntry/${id}/withdraw`,
    method: 'post'
  })
}
/**
 * 审核采购入库
 * @param params
 */
export function auditPurchaseWarehousing(id, parmas) {
  return request({
    url: `admin/erp/warehouseEntry/${id}/audit`,
    method: 'post',
    data: parmas
  })
}

/**
 * 获取采购入库详情
 * @param params
 */
export function getPurchaseWarehousingInfo(id) {
  return request({
    url: `admin/erp/warehouseEntry/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 获取采购入库列表
 * @param params
 */
export function getPurchaseWarehousingList(params) {
  return request({
    url: 'admin/erp/warehouseEntry',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 添加采购入库
 * @param params
 */
export function addPurchaseWarehousing(parmas) {
  return request({
    url: `admin/erp/warehouseEntry`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 更新采购入库
 * @param id
 * @param parmas
 */
export function editPurchaseWarehousing(id, parmas) {
  return request({
    url: `admin/erp/warehouseEntry/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除采购入库
 * @param id
 */
export function deletePurchaseWarehousing(ids) {
  return request({
    url: `admin/erp/warehouseEntry/${ids}`,
    method: 'delete'
  })
}

/**
 * 获取合同详情
 * @param params
 */
export function getContractInfo(id) {
  return request({
    url: `admin/procurement/contract/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 获取合同列表
 * @param params
 */
export function getContractList(params) {
  return request({
    url: 'admin/procurement/contract',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 添加合同
 * @param params
 */
export function addContract(parmas) {
  return request({
    url: `admin/procurement/contract`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 更新合同
 * @param id
 * @param parmas
 */
export function editContract(id, parmas) {
  return request({
    url: `admin/procurement/contract/${id}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 执行合同
 * @param id
 */
export function executeContract(ids) {
  return request({
    url: `admin/procurement/contract/execute/${ids}`,
    method: 'post'
  })
}

/**
 * 关闭合同
 * @param id
 */
export function closeContract(ids) {
  return request({
    url: `admin/procurement/contract/close/${ids}`,
    method: 'post'
  })
}

/**
 * 删除合同
 * @param id
 */
export function deleteContract(ids) {
  return request({
    url: `admin/procurement/contract/${ids}`,
    method: 'delete'
  })
}

/**
 * 获取采购计划详情
 * @param params
 */
export function getPurchasePlanInfo(id) {
  return request({
    url: `admin/procurement/plan/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 获取采购计划列表
 * @param params
 */
export function getPurchasePlanList(params) {
  return request({
    url: 'admin/procurement/plan',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 添加采购计划
 * @param params
 */
export function addPurchasePlan(parmas) {
  return request({
    url: `admin/procurement/plan`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 更新采购计划
 * @param id
 * @param parmas
 */
export function editPurchasePlan(id, parmas) {
  return request({
    url: `admin/procurement/plan/${id}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除采购计划
 * @param id
 */
export function deletePurchasePlan(ids) {
  return request({
    url: `admin/procurement/plan/${ids}`,
    method: 'delete'
  })
}
