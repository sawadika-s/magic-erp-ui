/**
 * 统计相关API
 */

import request from '@/utils/request'

/**
 * 导出库存成本列表
 * @param params
 */
export function getstockCostExport(params) {
  return request({
    url: 'admin/stock/statistics/export-cost',
    method: 'get',
    responseType: 'blob',
    loaidng: false,
    params
  })
}

/**
 * 查询库存成本统计分页列表数据
 * @param params
 */
export function getstockCostStatistics(params) {
  return request({
    url: 'admin/stock/statistics/cost',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 导出库存列表
 * @param params
 */
export function getstockExport(params) {
  return request({
    url: 'admin/stock/statistics/export',
    method: 'get',
    responseType: 'blob',
    loaidng: false,
    params
  })
}

/**
 * 查询库存统计分页列表数据
 * @param params
 */
export function getstockStatistics(params) {
  return request({
    url: 'admin/stock/statistics',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 导出采购列表
 * @param params
 */
export function getwarehouseEntryExport(params) {
  return request({
    url: 'admin/erp/warehouseEntry/export',
    method: 'get',
    responseType: 'blob',
    loaidng: false,
    params
  })
}

/**
 * 查询采购统计分页列表数据
 * @param params
 */
export function getwarehouseEntryStatistics(params) {
  return request({
    url: 'admin/erp/warehouseEntry/statistics',
    method: 'get',
    loaidng: false,
    params
  })
}
/**
 * 获取某商品入库统计
 * @param {*} params
 * @returns
 */
export function getStockBatchFlow(params) {
  return request({
    url: 'admin/erp/stockBatchFlow',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 导出供应商退货列表
 * @param params
 */
export function getsupplierReturnExport(params) {
  return request({
    url: 'admin/erp/supplierReturn/export',
    method: 'get',
    responseType: 'blob',
    loaidng: false,
    params
  })
}

/**
 * 查询供应商退货统计分页列表数据
 * @param params
 */
export function getsupplierReturnStatistics(params) {
  return request({
    url: 'admin/erp/supplierReturn/statistics',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 导出出库列表
 * @param params
 */
export function getwarehouseOutExport(params) {
  return request({
    url: 'admin/erp/warehouseOut/export',
    method: 'get',
    responseType: 'blob',
    loaidng: false,
    params
  })
}

/**
 * 查询出库统计分页列表数据
 * @param params
 */
export function getwarehouseOutStatistics(params) {
  return request({
    url: 'admin/erp/warehouseOut/statistics',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 导出订单退货列表
 * @param params
 */
export function getorderReturnExport(params) {
  return request({
    url: 'admin/erp/orderReturn/export',
    method: 'get',
    responseType: 'blob',
    loaidng: false,
    params
  })
}

/**
 * 查询订单退货统计分页列表数据
 * @param params
 */
export function getorderReturnStatistics(params) {
  return request({
    url: 'admin/erp/orderReturn/statistics',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 导出调拨列表
 * @param params
 */
export function getStockTransferExport(params) {
  return request({
    url: 'admin/erp/stockTransfer/export',
    method: 'get',
    responseType: 'blob',
    loaidng: false,
    params
  })
}

/**
 * 查询调拨统计分页列表数据
 * @param params
 */
export function getstockTransferStatistics(params) {
  return request({
    url: 'admin/erp/stockTransfer/statistics',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 导出库存调整单商品列表
 * @param params
 */
export function getStockDamageReportExport(params) {
  return request({
    url: 'admin/stock/damage/report/export',
    method: 'get',
    responseType: 'blob',
    loaidng: false,
    params
  })
}

/**
 * 查询库存调整单商品统计分页列表数据
 * @param params
 */
export function getStockDamageReportStatistics(params) {
  return request({
    url: 'admin/stock/damage/report/statistics',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 导出商品换货单商品列表
 * @param params
 */
export function getChangeFormExport(params) {
  return request({
    url: 'admin/change/form/export',
    method: 'get',
    loaidng: false,
    responseType: 'blob',
    params
  })
}

/**
 * 查询商品换货单商品统计分页列表数据
 * @param params
 */
export function getChangeFormStatistics(params) {
  return request({
    url: 'admin/change/form/statistics',
    method: 'get',
    loaidng: false,
    params
  })
}
