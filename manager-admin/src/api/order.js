/**
 * 订单相关API
 */

import request from '@/utils/request'

/**
 * 出库单审核
 * @param params
 */
export function handleAuditWarehouseOut(ids, params) {
  return request({
    url: `/admin/erp/warehouseOut/audit/${ids}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    params
  })
}

/**
 * 调拨单商品明细
 * @param params
 */
export function getWarehouseOutStockTransferProduct(params) {
  return request({
    url: 'admin/erp/stockTransferProduct',
    method: 'get',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    params
  })
}

/**
 * 获取物流公司列表
 * @param params
 * @returns {Promise<any>}
 */
export function getExpressCompanyList(params) {
  return request({
    url: 'admin/systems/logi-companies',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 发货出库单
 * @param params
 */
export function warehouseOutShip(parmas) {
  return request({
    url: `admin/erp/warehouseOut/ship`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 获取出库单详情
 * @param params
 */
export function getWarehouseOutInfo(id) {
  return request({
    url: `admin/erp/warehouseOut/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 获取出库单列表
 * @param params
 */
export function getWarehouseOutList(params) {
  return request({
    url: 'admin/erp/warehouseOut',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 更新出库单
 * @param id
 * @param parmas
 */
export function editWarehouseOut(id, params) {
  return request({
    url: `admin/erp/warehouseOut/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 批量删除出库单
 * @param id
 */
export function deleteWarehouseOut(ids) {
  return request({
    url: `admin/erp/warehouseOut/${ids}`,
    method: 'delete'
  })
}

/**
 * 出库单添加
 * @param params
 */
export function addErpWarehouseOut(params) {
  return request({
    url: 'admin/erp/warehouseOut',
    method: 'post',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 出库前预览
 * @param params
 */
export function getWarehouseOutPreview(params) {
  return request({
    url: 'admin/erp/warehouseOut/preview',
    method: 'post',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 获取订单列表
 * @param params
 */
export function getOrderList(params) {
  return request({
    url: 'admin/erp/order',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 根据订单id获取订单详情
 * @param order_id
 */
export function getOrderDetail(order_id) {
  return request({
    url: `admin/erp/order/${order_id}`,
    method: 'get',
    loading: false
  })
}

/**
 * 添加订单
 * @param order
 */
export function addOrder(order) {
  return request({
    url: `admin/erp/order`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: order
  })
}

/**
 * 修改订单
 * @param id
 * @param order
 */
export function editOrder(id, order) {
  return request({
    url: `admin/erp/order/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: order
  })
}

/**
 * 获取推送记录
 * @param params
 */
export function syncLogList(params) {
  return request({
    url: 'admin/datasync/messagePush',
    method: 'get',
    params
  })
}

/**
 * 同步记录-->再次推送
 * @param params
 */
export function syncLogAgain(id) {
  return request({
    url: `admin/datasync/messagePush/${id}/push`,
    method: 'get'
  })
}

/**
 * 提交订单
 * @param id
 */
export function submit(id) {
  return request({
    url: `admin/erp/order/${id}/submit`,
    method: 'post'
  })
}

/**
 * 撤回订单
 * @param id
 */
export function withdraw(id) {
  return request({
    url: `admin/erp/order/${id}/withdraw`,
    method: 'post'
  })
}

/**
 * 审核订单
 * @param id
 */
export function audit(id, isPass, remark) {
  return request({
    url: `admin/erp/order/${id}/audit`,
    method: 'post',
    params: { is_pass: isPass, remark }
  })
}

/**
 * 设为已支付
 * @param id
 */
export function payment(id) {
  return request({
    url: `admin/erp/order/${id}/payment`,
    method: 'post'
  })
}
