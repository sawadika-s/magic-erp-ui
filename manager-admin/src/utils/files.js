import axios from 'axios'
/**
 *
 * @param {name:中文名，url：后台英文拼接路径} templateNameConfig
 */
export function downloadFile(templateConfig) {
  let { name, url } = templateConfig

  axios({
    method: 'get',
    url: url,
    responseType: 'blob'
  }).then(res => {
    const suffix = url.slice(url.lastIndexOf('.') + 1)
    let objectURL = window.URL.createObjectURL(res.data)
    let link = document.createElement('a')
    link.style.display = 'none'
    link.href = objectURL
    link.setAttribute('id', 'downloadLink')
    link.setAttribute('download', `${name}.${suffix}`)
    document.body.appendChild(link)
    link.click()
    // 删除添加的a链接
    let objLink = document.getElementById('downloadLink')
    document.body.removeChild(objLink)
  })
}
