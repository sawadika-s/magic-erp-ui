/**
 * Created by Andste on 2021/8/18.
 * 采购计划选择器
 * 依赖于element-ui
 */
import Vue from 'vue'
import PickerPlan from './src/main.vue'
import PickerPlanPicker from './src/main.js'

PickerPlan.install = function() {
  Vue.component(PickerPlan.name, PickerPlan)
}
Vue.prototype.$EnPickerPlan = PickerPlanPicker

export default PickerPlan
