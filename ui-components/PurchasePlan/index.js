/**
 * Created by Andste on 2021/8/18.
 * 采购计划选择器
 * 依赖于element-ui
 */
import Vue from 'vue'
import PurchasePlan from './src/main.vue'
import purchasePlanPicker from './src/main.js'

PurchasePlan.install = function() {
  Vue.component(PurchasePlan.name, PurchasePlan)
}
Vue.prototype.$EnPurchasePlan = purchasePlanPicker

export default PurchasePlan
