/**
 * Created by Andste on 2021/8/18.
 * 供应商选择器
 * 依赖于element-ui
 */
import Vue from 'vue'
import PickerSupplier from './src/main.vue'
import PickerSupplierPicker from './src/main.js'

PickerSupplier.install = function() {
  Vue.component(PickerSupplier.name, PickerSupplier)
}
Vue.prototype.$EnPickerSupplier = PickerSupplierPicker

export default PickerSupplier
