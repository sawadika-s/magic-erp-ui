import request from '@/utils/request'

export const props = {
  // 显示选择器
  show: {
    type: Boolean,
    default: false
  },
  // 已选导购的ID集合
  selectedIds: {
    type: Array,
    required: false,
    default: () => ([])
  },
  // 获取导购列表API
  goodsApi: {
    type: String,
    required: true
  },
  // 扩展的导购API参数
  goodsApiParams: {
    type: Object,
    default: () => ({})
  },
  // 请求方法
  request: {
    type: Function,
    required: false,
    default: request
  },
  // 最大可选个数
  limit: {
    type: Number,
    default: -1
  },
  // 扩展列
  columns: {
    type: Array,
    default: () => (generalColumns)
  },
  // 是否是管理端
  isAdmin: {
    type: Boolean,
    default: true
  }
}

export const data = {
}

// 一般列
export const generalColumns = [
  { label: '供应商名称', prop: 'custom_name' },
  { label: '供应商编码', prop: 'custom_sn' },
  { label: '供应商地址', prop: 'production_address' },
  { label: '传真', prop: 'transfer' },
  { label: '联系人', prop: 'linkman' },
  { label: '电话', prop: 'telephone' },
  { label: '电子邮件', prop: 'email' }
] 
